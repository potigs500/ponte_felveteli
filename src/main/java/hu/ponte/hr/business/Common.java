package hu.ponte.hr.business;

import java.util.function.Function;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * It contains common codes for the system
 * @author apotor
 *
 */
public class Common {
	
	private Common() {}
	
	/**
	 * You can convert anything from A to B
	 * @param <A>
	 * @param <B>
	 * @param a
	 * @param function
	 * @return
	 */
	public static <A,B> B converter(A a, Function<A, B> function) {
		return function.apply(a);
	}
	
	/**
	 * It does Json string
	 * @param element
	 * @return
	 * @throws JsonProcessingException
	 */
	public static String toJSON(Object element) throws JsonProcessingException {
		return new ObjectMapper().writeValueAsString(element);
	}
	
	
}
