package hu.ponte.hr.business.enums;

/**
 * It stores the system messages. 
 * @author apotor 2020.09.19
 */
public enum Message {
	ERROR_FILE_IS_EMPTY("File content is empty"),
	ERROR_FILE_SIZE("The maximum of file size is 2Mb!"),
	ERROR_FILE_MIME_TYPE("Just image approved!"),
	ERROR_FILE_ENCODEING("Error during digital sign"),
	ERROR_FILE_PROCESS("Error during the file process"),
	ERROR_BACKGROUND_ERROR("Backround error"),
	WARNING_RESULT_IS_EMPTY("Result is empty"),
	SUCCESS_OPERATION("The operation was successful");
	
	private String message;
	
	Message(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		return this.message;
	}
}
