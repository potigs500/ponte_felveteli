package hu.ponte.hr.controller;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author zoltan
 */
@Getter
@Setter
@NoArgsConstructor
public class ImageMeta
{
	private long id;
	private String name;
	private String mimeType;
	private long size;
	private String digitalSign;
}
