package hu.ponte.hr.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hu.ponte.hr.exceptions.EmptyElementException;
import hu.ponte.hr.services.ImageStore;
import hu.ponte.hr.services.file.FileProcessorEncoderImpl;

/**
 * It is the rest endpoint.
 * You can list images or query one image by id 
 * @author apotor
 *
 */
@RestController()
@RequestMapping("api/images")
public class ImagesController {
	
	private Logger log = LoggerFactory.getLogger(FileProcessorEncoderImpl.class);

    @Autowired
    private ImageStore imageStore;

    @GetMapping("meta")
    public List<ImageMeta> listImages() {
    	log.info("meta endpoint calling.");
    	return imageStore.getAllFiles();
    }

    @GetMapping("preview/{id}")
    public ResponseEntity<ImageMeta> getImage(@PathVariable("id") String id, HttpServletResponse response) {
    	log.info("preview/{id} endpoint calling.");
		try {
			return ResponseEntity.ok().body(imageStore.getFileById(id));
		} catch (EmptyElementException e) {
			return ResponseEntity
					.status(HttpStatus.NO_CONTENT)
					.body(null);
		}
	
	}

}
