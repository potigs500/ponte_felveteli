package hu.ponte.hr.controller.upload;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import hu.ponte.hr.business.Common;
import hu.ponte.hr.business.enums.Message;
import hu.ponte.hr.services.ImageStore;

/**
 * It is the rest enpoint of the file upload
 * @author apotor
 *
 */
@Component
@RequestMapping("api/file")
public class UploadController {
	
	private Logger log = LoggerFactory.getLogger(UploadController.class);
	
	@Autowired
	private ImageStore imageStore;
	
    @RequestMapping(value = "post", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> handleFormUpload(@RequestParam("file") MultipartFile file) throws IOException {
    	try {
    		log.info("File upload start");
			imageStore.fileStore(file);
			log.info("File upload success");
			return ResponseEntity.ok().body(Common.toJSON(Message.SUCCESS_OPERATION.toString()));
		} catch (IOException e) {
			log.error(e.getMessage());
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Common.toJSON(e.getMessage()));
		} catch (Exception e) {
			log.error(e.getMessage());
			return ResponseEntity
					.status(HttpStatus.INTERNAL_SERVER_ERROR)
					.body(Common.toJSON(Message.ERROR_BACKGROUND_ERROR.toString()));
		}
    }
}
