package hu.ponte.hr.dao;

import org.springframework.data.repository.CrudRepository;
import hu.ponte.hr.entity.ImageMetaEntity;

/**
 * It serves database operations for handle image requests. 
 * @author apotor
 *
 */
public interface ImageRepository extends CrudRepository<ImageMetaEntity, Long>{
	
	ImageMetaEntity findImageMetaEntityByDigitalSign(String digitalSign);
	ImageMetaEntity findImageMetaEntityByName(String name);
	
}
