package hu.ponte.hr.exceptions;

public class EmptyElementException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public EmptyElementException(String message) {
		super(message);
	}
	
}