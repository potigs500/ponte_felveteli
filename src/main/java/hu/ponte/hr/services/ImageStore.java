package hu.ponte.hr.services;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.ponte.hr.business.Common;
import hu.ponte.hr.business.enums.Message;
import hu.ponte.hr.controller.ImageMeta;
import hu.ponte.hr.dao.ImageRepository;
import hu.ponte.hr.entity.ImageMetaEntity;
import hu.ponte.hr.exceptions.EmptyElementException;
import hu.ponte.hr.services.file.FileProcessor;

/**
 * It will be serve the business logic releated to image operations.
 * @author apotor
 *
 */
@Service
public class ImageStore {

	private Logger log = LoggerFactory.getLogger(ImageStore.class);

	@Autowired
	private ImageRepository imageRepository;

	@Autowired
	private FileProcessor processor;

	@Autowired
	private ModelMapper modelMapper;

	@Transactional
	public void fileStore(MultipartFile file) throws IOException {
		processor.fileChecker(file);

		ImageMetaEntity entityFromFile = getEntityFromFile(file);
		ImageMetaEntity entityInDb = imageRepository.findImageMetaEntityByDigitalSign(entityFromFile.getDigitalSign());

		if (entityInDb == null) {
			log.info("Image is not in database, it will be saved " + entityFromFile.getName());
			imageRepository.save(entityFromFile);
		}
	}

	@Transactional
	public ImageMeta getFileById(String id) throws EmptyElementException {
		log.info("Search image by id: " + id);

		Optional<ImageMetaEntity> entity = imageRepository.findById(Long.parseLong(id));
		if (!entity.isPresent()) {
			log.warn("Image not found with id: " + id);
			throw new EmptyElementException(Message.WARNING_RESULT_IS_EMPTY.toString());
		}

		log.info("Image found with id " + entity.get().getId());
		return Common.converter(entity, (dto) -> modelMapper.map(entity.get(), ImageMeta.class));

	}

	@Transactional
	public List<ImageMeta> getAllFiles() {
		log.info("All images collect from db");
		return StreamSupport.stream(imageRepository.findAll().spliterator(), false).map((ImageMetaEntity entity) -> {
			return Common.converter(entity, (dto) -> modelMapper.map(entity, ImageMeta.class));
		}).collect(Collectors.toList());
	}
	
	/**
	 * It creates ImageMetaEntity from uploaded file
	 * @param file The uploaded file
	 * @return
	 * @throws IOException
	 */
	private ImageMetaEntity getEntityFromFile(MultipartFile file) throws IOException {
		ImageMetaEntity entity = new ImageMetaEntity();

		entity.setName(file.getResource().getFilename());
		entity.setSize(file.getSize());
		entity.setDigitalSign(processor.encode(file));
		entity.setMimeType(file.getContentType());

		log.info("Image save start with image: " + file.getResource().getFilename());
		return entity;
	}

}
