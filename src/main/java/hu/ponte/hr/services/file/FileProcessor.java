package hu.ponte.hr.services.file;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

public interface FileProcessor  {

	void save(String path, File file)  throws IOException;
	void delete(String path, File file) throws IOException;
	void read(String path, File file) throws IOException;
	void save(String path, MultipartFile file)  throws IOException;
	void delete(String path, MultipartFile file) throws IOException;
	void read(String path, MultipartFile file) throws IOException;
	void fileChecker(File file)  throws IOException;
	void fileChecker(MultipartFile file) throws IOException;
	String encode(File file) throws IOException;
	String encode(MultipartFile file) throws IOException;
	
}
