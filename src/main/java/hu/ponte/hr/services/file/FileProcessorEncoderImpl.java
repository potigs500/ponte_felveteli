package hu.ponte.hr.services.file;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import hu.ponte.hr.business.enums.Message;

/**
 * This class processes the file.
 * You can save the file into the file system 
 * or encode it with digital sign. 
 * @author apotor 2020.09.19
 *
 */
@Service
public class FileProcessorEncoderImpl implements FileProcessor {
	
	private Logger log = LoggerFactory.getLogger(FileProcessorEncoderImpl.class);
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Autowired 
	Environment env;
	
	/**
	 * It encodes a simple file. TODO: Need to implement
	 */
	@Override
	public String encode(File file) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * It encodes a multipart file. 
	 * It signs the file with SHA256withRSA algorithm
	 * and then it will be encoded BASE64
	 * param: MultipartFile file 
	 * return: encoded file
	 */
	@Override
	public String encode(MultipartFile file) throws IOException {
		log.info("Encode procedure start on the file");
		String path = env.getProperty("hu.ponte.hr.encoding.keys.key.private");
		//Path path = resourceLoader.getResource("classpath:/public/key.private").getFile().toPath();
		//Path path2 = resourceLoader.getResource("classpath:/key.private").getFile().toPath();
		//System.err.println(path.getFileName());

		byte[] privateKey = Files.readAllBytes(Paths.get(path));
		byte[] imageBytes = file.getBytes();
		
		return signSHA256RSA(imageBytes, privateKey);
	}
	
	/**
	 * It saves the file somewhere
	 * TODO:  Need to implement
	 */
	@Override
	public void save(String path, File file) throws IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * It saves the Multipart file somewhere
	 * TODO:  Need to implement
	 */
	@Override
	public void save(String path, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * It deletes the file from somewhere. 
	 * TODO:  Need to implement
	 * @param String path from to delete
	 * @param Simple file
	 */
	@Override
	public void delete(String path, File file) throws IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * It deletes the Multipart file from somewhere. 
	 * TODO:  Need to implement
	 * @param String path from to delete
	 * @param MultipartFile file
	 */
	@Override
	public void delete(String path, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * It reads the file from somewhere. 
	 * TODO:  Need to implement
	 * @param String path from to read
	 * @param Simple file
	 */
	@Override
	public void read(String path, File file) throws IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * It reads the Multipart file from somewhere. 
	 * TODO:  Need to implement
	 * @param String path from to read
	 * @param MultipartFile file
	 */
	@Override
	public void read(String path, MultipartFile file) throws IOException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * It checks the file properties. 
	 * TODO:  Need to implement
	 * @param Simle file
	 */
	@Override
	public void fileChecker(File file) throws IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * It checks Multipart file properties. 
	 * If the file is null, empty, too big or
	 * mime type is not appropriate, you will get
	 * exception
	 * 
	 * @param: MultipartFile file
	 * @throws: IOException
	 */
	@Override
	public void fileChecker(MultipartFile file) throws IOException {
		log.info("File checking start");
		if(file == null || file.isEmpty()) {
			throw new IOException(Message.ERROR_FILE_IS_EMPTY.toString());
		} else if(!file.getContentType().startsWith("image")) {
			throw new IOException(Message.ERROR_FILE_MIME_TYPE.toString());
		} else if(file.getSize() > 2097152) {
			throw new IOException(Message.ERROR_FILE_SIZE.toString());
		}
		log.info("File checking end");
	}
	
	/**
	 * It encodes a multipart file. 
	 * It signs the file with SHA256withRSA algorithm
	 * and then it will be encoded BASE64.
	 * @param: byte[] imageBytes The byte representation of the file
	 * @param: byte[] privateKey The byte representation of the secret key
	 */
	private String signSHA256RSA(byte[] imageBytes, byte[] privateKey) throws IOException {
		log.info("The encryption process start ");
        Signature privateSignature;
        KeyFactory keyFactory;
        PKCS8EncodedKeySpec spec;
		try {
			privateSignature = Signature.getInstance(env.getProperty("hu.ponte.hr.encoding.signature"));
			keyFactory = KeyFactory.getInstance(env.getProperty("hu.ponte.hr.encoding.keyfactory"));
			spec = new PKCS8EncodedKeySpec(privateKey);
			
			privateSignature.initSign(keyFactory.generatePrivate(spec));
		    privateSignature.update(imageBytes);
		    byte[] s = privateSignature.sign();
		    
	        String base64EncodedFile = Base64.getEncoder().encodeToString(s);
	    	log.info("Encode procedure end on the file");
	    	return base64EncodedFile;
		} catch (NoSuchAlgorithmException e) {
			log.error(e.getMessage());
			throw new IOException(Message.ERROR_FILE_ENCODEING.toString());
		} catch (InvalidKeyException e) {
			log.error(e.getMessage());
			throw new IOException(Message.ERROR_FILE_ENCODEING.toString());
		} catch (InvalidKeySpecException e) {
			log.error(e.getMessage());
			throw new IOException(Message.ERROR_FILE_ENCODEING.toString());
		} catch (SignatureException e) {
			log.error(e.getMessage());
			throw new IOException(Message.ERROR_FILE_ENCODEING.toString());
		}
    }

}
