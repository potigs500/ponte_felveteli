package hu.ponte.hr.services;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import javax.transaction.Transactional;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import hu.ponte.hr.controller.ImageMeta;
import hu.ponte.hr.exceptions.EmptyElementException;

@RunWith(SpringRunner.class)
@SpringBootTest()
@FixMethodOrder(MethodSorters.NAME_ASCENDING) //@Order() nem működött
public class ImageStoreTest {

	@Autowired
	private ImageStore store;

	private static final String TEST_IMAGE_PATH = "src/test/resources/images/";
	private static final String IMAGE_NAME = "cat.jpg";

	@Transactional
	@Test
	public void save_and_get_file() throws IOException, EmptyElementException {
		MockMultipartFile file = getTestMockMultipartFile(TEST_IMAGE_PATH, IMAGE_NAME);
		store.fileStore(file);
		
		ImageMeta meta = store.getFileById("1");
		
		assertEquals(file.getName(), meta.getName());
		assertEquals(file.getSize(), meta.getSize());
	
	}
	
	private MockMultipartFile getTestMockMultipartFile(String rootPath, String fileName) throws IOException {
		byte[] fileContent = Files.readAllBytes(Paths.get(rootPath+fileName));
		return new MockMultipartFile(fileName, fileName, "image/jpeg",fileContent);
	}

}
