package hu.ponte.hr.services.file;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import hu.ponte.hr.business.enums.Message;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class FileProcessorEncoderTest {
	
	private static final String TEST_FILE_PATH = "src/test/resources/files/";
	private static final String TEST_IMAGE_PATH = "src/test/resources/images/";
	
	private Map<String, String> testImages = new LinkedHashMap<String, String>() {
		{
			put("cat.jpg","XYZ+wXKNd3Hpnjxy4vIbBQVD7q7i0t0r9tzpmf1KmyZAEUvpfV8AKQlL7us66rvd6eBzFlSaq5HGVZX2DYTxX1C5fJlh3T3QkVn2zKOfPHDWWItdXkrccCHVR5HFrpGuLGk7j7XKORIIM+DwZKqymHYzehRvDpqCGgZ2L1Q6C6wjuV4drdOTHps63XW6RHNsU18wHydqetJT6ovh0a8Zul9yvAyZeE4HW7cPOkFCgll5EZYZz2iH5Sw1NBNhDNwN2KOxrM4BXNUkz9TMeekjqdOyyWvCqVmr5EgssJe7FAwcYEzznZV96LDkiYQdnBTO8jjN25wlnINvPrgx9dN/Xg==");
			put("enhanced-buzz.jpg","tsLUqkUtzqgeDMuXJMt1iRCgbiVw13FlsBm2LdX2PftvnlWorqxuVcmT0QRKenFMh90kelxXnTuTVOStU8eHRLS3P1qOLH6VYpzCGEJFQ3S2683gCmxq3qc0zr5kZV2VcgKWm+wKeMENyprr8HNZhLPygtmzXeN9u6BpwUO9sKj7ImBvvv/qZ/Tht3hPbm5SrDK4XG7G0LVK9B8zpweXT/lT8pqqpYx4/h7DyE+L5bNHbtkvcu2DojgJ/pNg9OG+vTt/DfK7LFgCjody4SvZhSbLqp98IAaxS9BT6n0Ozjk4rR1l75QP5lbJbpQ9ThAebXQo+Be4QEYV/YXf07WXTQ==");
			put("rnd.jpg","lM6498PalvcrnZkw4RI+dWceIoDXuczi/3nckACYa8k+KGjYlwQCi1bqA8h7wgtlP3HFY37cA81ST9I0X7ik86jyAqhhc7twnMUzwE/+y8RC9Xsz/caktmdA/8h+MlPNTjejomiqGDjTGvLxN9gu4qnYniZ5t270ZbLD2XZbuTvUAgna8Cz4MvdGTmE3MNIA5iavI1p+1cAN+O10hKwxoVcdZ2M3f7/m9LYlqEJgMnaKyI/X3m9mW0En/ac9fqfGWrxAhbhQDUB0GVEl7WBF/5ODvpYKujHmBAA0ProIlqA3FjLTLJ0LGHXyDgrgDfIG/EDHVUQSdLWsM107Cg6hQg==");
		}
	};
	
	
	@Autowired
	private FileProcessor processor;
	
	@Test(expected = IOException.class)
	public void throw_exception_if_file_is_null() throws IOException {
		MockMultipartFile file = null;
		processor.fileChecker(file);
	}
	
	@Test
	public void throw_exception_if_file_is_empty() {
		Exception exception = assertThrows(IOException.class, () -> {
			processor.fileChecker(getTestMockMultipartFile(TEST_FILE_PATH, "empty_file.txt"));
		});
		assertEquals(Message.ERROR_FILE_IS_EMPTY.toString(),exception.getMessage());
	}

	@Test
	public void throw_exception_if_file_mime_type_not_image() {
		Exception exception = assertThrows(IOException.class, () -> {
			byte[] fileContent = Files.readAllBytes(Paths.get(TEST_FILE_PATH+"file_with_content.txt"));
			MockMultipartFile file = new MockMultipartFile("file_with_content.txt", null, "text/plain",fileContent);
			processor.fileChecker(file);
		});
		assertEquals(Message.ERROR_FILE_MIME_TYPE.toString(),exception.getMessage());
	}
	
	@Test
	public void throw_exception_if_file_size_too_big() {
		Exception exception = assertThrows(IOException.class, () -> {
			byte[] fileContent = Files.readAllBytes(Paths.get(TEST_IMAGE_PATH+"big_image_3mb.jpg"));
			MockMultipartFile file = new MockMultipartFile("big_image_3mb.jpg", null, "image/jpeg",fileContent);
			processor.fileChecker(file);
		});
		assertEquals(Message.ERROR_FILE_SIZE.toString(),exception.getMessage());
	}
	
	@Test
	public void encode_process_test() throws IOException {
		for(String key : testImages.keySet()) {
			String base64EncodedImage =  processor.encode(getTestMockMultipartFile(TEST_IMAGE_PATH,key));
			assertEquals(testImages.get(key), base64EncodedImage);
		}
	}
	
	private MockMultipartFile getTestMockMultipartFile(String rootPath, String fileName) throws IOException {
		byte[] fileContent = Files.readAllBytes(Paths.get(rootPath+fileName));
		return new MockMultipartFile(fileName,fileContent);
	}
	
}
